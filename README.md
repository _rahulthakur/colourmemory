# ColourMemory #

**Platform:** Android

**API:** 16+ *(v4.1.1)*

**Build Tool:** 21.1.2

**Category:** Game

**Objective:** Match all cards

**Demo Video:** [http://youtu.be/FKqzqesZa-Y](http://youtu.be/FKqzqesZa-Y)

**APK:** [http://www.amazon.com/Rahul-Thakur-Colour-Memory/dp/B00ULPNBFK/](http://www.amazon.com/Rahul-Thakur-Colour-Memory/dp/B00ULPNBFK/)